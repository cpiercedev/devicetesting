//
//  sendLog.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import Firebase
import UIKit

var totalUpdated = false

var testResults: [String] = []


let IMEI = userDefaults.string(forKey: "IMEI")
let Serial = userDefaults.string(forKey: "Serial")
let WO = userDefaults.string(forKey: "WO")
let SystemVersion = userDefaults.string(forKey: "iOSVersion")
let StoreNumber = (userDefaults.string(forKey: "StoreNumber") ?? "N/A")
let Model = userDefaults.string(forKey: "model")
let StartTime = userDefaults.object(forKey: "StartTime")
let EndTime = Date()
let modelID = userDefaults.string(forKey: "modelID")

let LCD = userDefaults.string(forKey: "LCD")
let MultiTouch = userDefaults.string(forKey: "MultiTouch")
let Charging = userDefaults.string(forKey: "charging")
let RearCamera = userDefaults.string(forKey: "RearCamera")
let FrontCamera = userDefaults.string(forKey: "FrontCamera")
let Gyro = userDefaults.string(forKey: "Gyroscope")
let Speaker = userDefaults.string(forKey: "Speakers")
let IDAuth = userDefaults.string(forKey: "IDAuth")
let Loopback = userDefaults.string(forKey: "Loopback")
let Vibration = userDefaults.string(forKey: "Vibration")
let Touch3D = userDefaults.string(forKey: "3DTouch")
let TouchGrid = userDefaults.string(forKey: "TouchGrid")
let NFC = userDefaults.string(forKey: "NFC")
let Bluetooth = userDefaults.string(forKey: "Bluetooth")
let Proximity = userDefaults.string(forKey: "Prox")
let accel = userDefaults.string(forKey: "Accelerometer")
let Headphone = userDefaults.string(forKey: "Headphone")
let Ambient = userDefaults.string(forKey: "Ambient")
let Barometer = userDefaults.string(forKey: "Barometer")
let WRT = userDefaults.string(forKey: "WRT")
let SIM = userDefaults.string(forKey: "SIM")
let Wifi = userDefaults.string(forKey: "Wifi")
let Cell = userDefaults.string(forKey: "Cellular")
let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String
var tokenCount = 0

let DocID = userDefaults.string(forKey: "DocID")
let db = Firestore.firestore()
let settings = db.settings
let documentID = DocID ?? "N/A"

var result = "Pass"


var deviceID: String?
var IQCID: String?

var WOLink: String?

var ref: DocumentReference? = nil

var cell = userDefaults.bool(forKey: "CellularModel")

func sendLog(controller: UIViewController, completion: @escaping (Bool) -> Void){
    
    appendArray()
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    
    print(DocID ?? "N/A")
    
    
    
    var logSaved = false
    
    
    WOLink = "https://portal.ubif.net/pos/workorder/" + WO!
    updateTotal(result: result)
    getData(controller: controller) {(isSucceeded) in
        if isSucceeded {
            //it exists, do something
            sendData(controller: controller) {(isSucceeded) in
                if isSucceeded {
                    //it exists, do something
                    completion(true)
                    return
                } else {
                    //user does not exist, do something else
                    completion(false)
                    return
                }
            }
        } else {
            //user does not exist, do something else
            completion(false)
            return
        }
    }
    
    
}

func sendData(controller: UIViewController, completion: @escaping (Bool) -> Void){
    let  docRef1 = db.collection("Devices/\(deviceID!)/Workorders").document(WO!);
    
    docRef1.getDocument { (document, error) in
        if let document = document, document.exists {
            if (document.data()!["IQCID"] != nil){
                IQCID = document.data()!["IQCID"] as! String
            }
            ref = db.collection("logs").addDocument(data: [
                "StartTime": StartTime,
                "DateTime": Date(),
                "IMEI": IMEI ?? "N/A",
                "Serial": Serial ?? "N/A",
                "WO": WO!,
                "LCD": LCD,
                "MultiTouch": MultiTouch,
                "StoreNumber": StoreNumber,
                "Charging": Charging,
                "RearCamera": RearCamera,
                "FrontCamera": FrontCamera,
                "Gyroscope": Gyro,
                "Vibration": Vibration,
                "Speaker": Speaker,
                "Pass": result,
                "IDAuth": IDAuth,
                "Loopback": Loopback,
                "Accelerometer": accel,
                "NFC": NFC,
                "Bluetooth": Bluetooth,
                "Touch3D": Touch3D,
                "Headphone": Headphone,
                "Proximity": Proximity,
                "TouchGrid": TouchGrid,
                "Model": Model,
                "AmbientLight": Ambient,
                "Barometer": Barometer,
                "WRT": WRT,
                "SIM": SIM,
                "Wifi": Wifi,
                "Cellular": Cell,
                "WOLink": WOLink,
                "Version": version,
                "Identifier": modelID,
                "IQCID": IQCID
                //"iOS Version": SystemVersion
                
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                    //logSaved = false
                    completion(false)
                    return
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                    logID(result: result, logID: ref!.documentID)
                    completion(true)
                    return
                }
            }
        }
        else {
            ref = db.collection("logs").addDocument(data: [
                "StartTime": StartTime,
                "DateTime": Date(),
                "IMEI": IMEI ?? "N/A",
                "Serial": Serial ?? "N/A",
                "WO": WO!,
                "LCD": LCD,
                "MultiTouch": MultiTouch,
                "StoreNumber": StoreNumber,
                "Charging": Charging,
                "RearCamera": RearCamera,
                "FrontCamera": FrontCamera,
                "Gyroscope": Gyro,
                "Vibration": Vibration,
                "Speaker": Speaker,
                "Pass": result,
                "IDAuth": IDAuth,
                "Loopback": Loopback,
                "Accelerometer": accel,
                "NFC": NFC,
                "Bluetooth": Bluetooth,
                "Touch3D": Touch3D,
                "Headphone": Headphone,
                "Proximity": Proximity,
                "TouchGrid": TouchGrid,
                "Model": Model,
                "AmbientLight": Ambient,
                "Barometer": Barometer,
                "WRT": WRT,
                "SIM": SIM,
                "Wifi": Wifi,
                "Cellular": Cell,
                "WOLink": WOLink,
                "Version": version,
                "Identifier": modelID,
                "IQCID": IQCID
                //"iOS Version": SystemVersion
                
                
                
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                    //logSaved = false
                    completion(false)
                    return
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                    
                    logID(result: result, logID: ref!.documentID)
                    
                    completion(true)
                    return
                }
            }
        }
        
    }
}


func getData(controller: UIViewController, completion: @escaping (Bool) -> Void){
    
    let  docRef = db.collection("users").document(documentID);
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            
            //print("\(document.documentID) => \(document.data())")
            var cell = userDefaults.bool(forKey: "CellularModel")
            var deviceID: String?
            if(cell){
                deviceID = userDefaults.string(forKey: "IMEI")
            }
            else{
                deviceID = userDefaults.string(forKey: "Serial")
            }
            
            
            let  docRef1 = db.collection("Devices/\(deviceID!)/Workorders").document(WO!);
            docRef1.getDocument { (document1, error) in
                if let document1 = document1, document1.exists {
                    
                    var IQCToken = document1.data()!["IQCToken"] as! Bool
                    var IQC = document1.data()!["IQC"] as! Bool
                    
                    print(document.data()!["Tokens"] as! Int)
                    
                    tokenCount = document.data()!["Tokens"] as! Int
                    var uses = document.data()!["Uses"] as! Int
                    var passCount = document.data()!["PassCount"] as! Int
                    var failCount = document.data()!["FailCount"] as! Int
                    
                    if(result == "Pass"){
                        passCount += 1
                    }
                    else{
                        failCount += 1
                    }
                    
                    print("User has \(tokenCount) tokens")
                    if(!IQCToken && IQC){
                        tokenCount -= 1
                    }
                    uses += 1
                    print("\(tokenCount) Tokens's remaining")
                    docRef.updateData([
                        "Tokens": tokenCount,
                        "Uses": uses,
                        "PassCount": passCount,
                        "FailCount": failCount
                    ]) { err in
                        if let err = err {
                            print("Error updating document: \(err)")
                        } else {
                            print("Document successfully updated")
                            completion(true)
                            return
                        }
                    }
                    
                } else {
                    print("Document does not exist")
                     completion(false)
                    return
                }
                
            }
        }
        else {
            print("Document does not exist TU")
             completion(false)
            return
        }
        
        
        
    }
    
}



func appendArray(){
    
    if(cell){
        deviceID = userDefaults.string(forKey: "IMEI")
    }
    else{
        deviceID = userDefaults.string(forKey: "Serial")
    }
    
    testResults.append(LCD ?? "False")
    testResults.append(MultiTouch ?? "False")
    testResults.append(Charging ?? "False")
    testResults.append(RearCamera ?? "False")
    testResults.append(FrontCamera ?? "False")
    testResults.append(Gyro ?? "False")
    testResults.append(Speaker ?? "False")
    testResults.append(IDAuth ?? "False")
    testResults.append(Loopback ?? "False")
    testResults.append(Vibration ?? "False")
    testResults.append(Touch3D ?? "False")
    testResults.append(TouchGrid ?? "False")
    testResults.append(NFC ?? "False")
    testResults.append(Bluetooth ?? "False")
    testResults.append(Proximity ?? "False")
    testResults.append(accel ?? "False")
    testResults.append(Headphone ?? "False")
    testResults.append(Ambient ?? "False")
    testResults.append(Barometer ?? "False")
    testResults.append(WRT ?? "False")
    testResults.append(SIM ?? "False")
    testResults.append(Wifi ?? "False")
    testResults.append(Cell ?? "False")
    
    for tests in testResults{
        if(tests == "Fail"){
            result = "Fail"
        }
    }
}
func updateTotal(result: String){
    if(StoreNumber == "1"){
        return
    }
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    let  docRef = db.collection("TotalUsage").document("Usage");
    
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            
            //var unlocks = document.data()!["TotalUnlocks"] as! Int
            var fail = document.data()!["TotalFail"] as! Int
            var pass = document.data()!["TotalPass"] as! Int
            var tokens = document.data()!["TotalTokens"] as! Int
            if(totalUpdated){
                return
            }
            totalUpdated = true
            if(result == "Pass"){
                pass += 1
            }
            else{
                fail += 1
            }
            tokens += 1
            
            docRef.updateData([
                "TotalTokens": tokens,
                "TotalFail": fail,
                "TotalPass": pass
                
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Total Document successfully updated")
                }
            }
            
        } else {
            print("Document does not exist TU")
        }
    }
}
func logID(result: String, logID: String){
    let userDefaults = UserDefaults.standard
    var cell = userDefaults.bool(forKey: "CellularModel")
    var deviceID: String?
    if(cell){
        deviceID = userDefaults.string(forKey: "IMEI")
    }
    else{
        deviceID = userDefaults.string(forKey: "Serial")
    }
    let WO = userDefaults.string(forKey: "WO")
    let StartTime = userDefaults.object(forKey: "StartTime")
    var Model = userDefaults.string(forKey: "model")
    
    let db = Firestore.firestore()
    let settings = db.settings
    var willUpdate = false
    var forcedResult = result
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    let  docRef1 = db.collection("Devices/\(deviceID!)/Workorders").document(WO!);
    
    docRef1.getDocument { (document, error) in
        if let document = document, document.exists {
            
            //var unlocks = document.data()!["TotalUnlocks"] as! Int
            var fail = document.data()!["FailCount"] as! Int
            var pass = document.data()!["PassCount"] as! Int
            
            if(result == "Pass"){
                pass += 1
            }
            else{
                fail += 1
            }
            
            docRef1.updateData([
                "PassCount": pass,
                "FailCount": fail,
                "EndTime": Date(),
                "Result": result,
                "LogID": logID,
                
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Total Document successfully updated")
                    }
            }
            
        } else {
            print("Document does not exist TU")
        }
    }
    
    
    
}
