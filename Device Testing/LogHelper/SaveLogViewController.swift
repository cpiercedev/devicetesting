//
//  SaveLogViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 1/21/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit

class SaveLogViewController: UIViewController {

    @IBOutlet var ActivityFrame: NVActivityIndicatorView!
    private let presentingIndicatorTypes = {
        return NVActivityIndicatorType.allCases.filter { $0 != .blank }
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        //ActivityFrame.startAnimating()
        // Do any additional setup after loading the view.
            
           
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        ActivityFrame.startAnimating()
        sendLog(controller: self) {(isSucceeded) in
            if isSucceeded {
                //it exists, do something
                self.SegueToResults()
            } else {
                //user does not exist, do something else
               
            }
        }
    }
    func SegueToResults(){
        ActivityFrame.stopAnimating()
        performSegue(withIdentifier: "SegueToResults", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
