//
//  ModelName.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/19/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
let userDefaults = UserDefaults.standard

public extension UIDevice {
    
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
            
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
          
            case "iPad2,1", "iPad2,4":                      return "iPad 2"
            case "iPad2,2", "iPad2,3":                      return "iPad 2 Cellular"
            case "iPad3,1":                                 return "iPad 3"
            case "iPad3,2", "iPad3,3":                      return "iPad 3 Cellular"
            case "iPad3,4":                                 return "iPad 4 Cellular"
            case "iPad3,5", "iPad3,6":                      return "iPad 4 Cellular"
            case "iPad4,1":                                 return "iPad Air"
            case "iPad4,2", "iPad4,3":                      return "iPad Air Cellular"
            case "iPad5,3":                                 return "iPad Air 2"
            case "iPad5,4":                                 return "iPad Air 2 Cellular"
            case "iPad6,11":                                return "iPad 5"
            case "iPad6,12":                                return "iPad 5 Cellular"
            case "iPad7,5":                                 return "iPad 6"
            case "iPad7,6":                                 return "iPad 6"
            case "iPad2,5":                                 return "iPad Mini"
            case "iPad2,6", "iPad2,7":                      return "iPad Mini Cellular"
            case "iPad4,4":                                 return "iPad Mini 2"
            case "iPad4,5", "iPad4,6":                      return "iPad Mini 2 Cellular"
            case "iPad4,7":                                 return "iPad Mini 3"
            case "iPad4,8", "iPad4,9":                      return "iPad Mini 3 Cellular"
            case "iPad5,1":                                 return "iPad Mini 4"
            case "iPad5,2":                                 return "iPad Mini 4 Cellular"
            case "iPad6,3":                                 return "iPad Pro 9.7 Inch"
            case "iPad6,4":                                 return "iPad Pro 9.7 Inch Cellular"
            case "iPad6,7":                                 return "iPad Pro 12.9 Inch"
            case "iPad6,8":                                 return "iPad Pro 12.9 Inch Cellular"
            case "iPad7,1":                                 return "iPad Pro 12.9 Inch 2. Generation"
            case "iPad7,2":                                 return "iPad Pro 12.9 Inch 2. Generation Cellular"
            case "iPad7,3":                                 return "iPad Pro 10.5 Inch"
            case "iPad7,4":                                 return "iPad Pro 10.5 Inch Cellular"
            case "iPad8,1", "iPad8,2":                      return "iPad Pro 11 Inch"
            case "iPad8,3", "iPad8,4":                      return "iPad Pro 11 Inch Cellular"
            case "iPad8,5", "iPad8,6":                      return "iPad Pro 12.9 Inch 3. Generation"
            case "iPad8,7", "iPad8,8":                      return "iPad Pro 12.9 Inch 3. Generation Cellular"
                
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "\(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    static let modelID: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier as String
    }()
    
    static let modelmAH: Int = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        func mapToDevice(identifier: String) -> Int { // swiftlint:disable:this cyclomatic_complexity
            switch identifier {
            case "iPod5,1":                                 return 1030
            case "iPod7,1":                                 return 1043
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return 1420
            case "iPhone4,1":                               return 1432
            case "iPhone5,1", "iPhone5,2":                  return 1440
            case "iPhone5,3", "iPhone5,4":                  return 1510
            case "iPhone6,1", "iPhone6,2":                  return 1560
            case "iPhone7,2":                               return 1810
            case "iPhone7,1":                               return 2915
            case "iPhone8,1":                               return 1715
            case "iPhone8,2":                               return 2750
            case "iPhone9,1", "iPhone9,3":                  return 1960
            case "iPhone9,2", "iPhone9,4":                  return 2900
            case "iPhone8,4":                               return 1624
            case "iPhone10,1", "iPhone10,4":                return 1821
            case "iPhone10,2", "iPhone10,5":                return 2691
            case "iPhone10,3", "iPhone10,6":                return 2716
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return 6579
            case "iPad3,1", "iPad3,2", "iPad3,3":           return 11487
            case "iPad3,4", "iPad3,5", "iPad3,6":           return 11560
            case "iPad4,1", "iPad4,2", "iPad4,3":           return 8827
            case "iPad5,3", "iPad5,4":                      return 7340
            case "iPad6,11", "iPad6,12":                    return 8827
            case "iPad7,5", "iPad7,6":                      return 8827
            case "iPad2,5", "iPad2,6", "iPad2,7":           return 4440
            case "iPad4,4", "iPad4,5", "iPad4,6":           return 6471
            case "iPad4,7", "iPad4,8", "iPad4,9":           return 6471
            case "iPad5,1", "iPad5,2":                      return 5124
            case "iPad6,3", "iPad6,4":                      return 7306
            case "iPad6,7", "iPad6,8":                      return 10307
            case "iPad7,1", "iPad7,2":                      return 10875
            case "iPad7,3", "iPad7,4":                      return 8134
            default:                                        return 0
            }
        }
        return mapToDevice(identifier: identifier)
        
    }()
    
    
}

func setModelName(){
    
    let device = UIDevice.current
    let model = UIDevice.modelName
    let modelID = UIDevice.modelID
    print(modelID)
    let BatteryMah: Float = Float(UIDevice.modelmAH)
    var systemVersion = UIDevice.current.systemVersion
    //var battery: BatteryInfo = BatteryInfo()
    let userDefaults = UserDefaults.standard
    userDefaults.set(model, forKey: "model")
    userDefaults.set(systemVersion, forKey: "iOSVersion")
    userDefaults.set(modelID, forKey: "modelID")
}
