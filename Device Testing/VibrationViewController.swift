//
//  VibrationViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/8/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import MediaPlayer

class VibrationViewController: UIViewController{
    
    var testName = "Vibration"
    
    
    
    @IBOutlet var oneButton: UIButton!
    @IBOutlet var twoButton: UIButton!
    @IBOutlet var threeButton: UIButton!
    @IBOutlet var startButton: UIButton!
    var number = 0
    var failCount = 0
    var startPressed = false
    var seguePath = "SegueToBluetooth"
    
    @IBOutlet var PassFailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        startButton.isEnabled = true
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        print(model)
        
        if (model == "iPhone 4" ||
            model == "iPhone 4s" ||
            model == "iPhone 5" ||
            model == "iPhone 5s" ||
            model == "iPhone 6"  ||
            model == "iPhone SE" ||
            model == "iPhone 6 Plus"  ||
            model == "iPhone 6s" ||
            model == "iPhone 6s Plus" ||
            model == "iPhone 7" ||
            model == "iPhone 7 Plus" ||
            model == "iPhone 8" ||
            model == "iPhone 8 Plus" ||
            model == "iPhone X")
        {
            print("it's in")
        }

        else{
            userDefaults.set("N/A", forKey: testName)
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        
        oneButton.isEnabled = false
        twoButton.isEnabled = false
        threeButton.isEnabled = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func VibrationStartPressed(_ sender: Any) {
        startPressed = true
        startButton.isEnabled = false
        oneButton.isEnabled = true
        twoButton.isEnabled = true
        threeButton.isEnabled = true
        number = Int.random(in: 1..<4)
        print("Button Pressed")
        var i = 0
        print(number)
        while i < number {
            sleep(1)
            vibrate()
            i = i + 1
            
        }

    }
        
        
        
        
        
        
        
    @IBAction func onePressed(_ sender: Any) {
        
        if(startPressed){
            
            oneButton.isEnabled = false
            twoButton.isEnabled = false
            threeButton.isEnabled = false
            if number == 1{
                PassFailLabel.text = "Pass"
                PassFailLabel.textColor = .green
                //sleep(1)
                let userDefaults = UserDefaults.standard
                print("true")
                userDefaults.set("Pass", forKey: testName)
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            else{
                failCount = failCount + 1
                startButton.isEnabled = true
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
            }
            if(failCount == 2){
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                //sleep(1)
               markFail()
            }
                startPressed = false
        }
    }
    @IBAction func twoPressed(_ sender: Any) {
        if(startPressed){
            oneButton.isEnabled = false
            twoButton.isEnabled = false
            threeButton.isEnabled = false
            if number == 2{
                PassFailLabel.text = "Pass"
                PassFailLabel.textColor = .green
                //sleep(1)
                let userDefaults = UserDefaults.standard
                print("true")
                userDefaults.set("Pass", forKey: testName)
                performSegue(withIdentifier: seguePath, sender: self)
            }
            else{
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                failCount = failCount + 1
                startButton.isEnabled = true
            }
            if(failCount == 2){
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                //sleep(1)
                let userDefaults = UserDefaults.standard
                markFail()
            }
                startPressed = false
        }
    }
    @IBAction func threePressed(_ sender: Any) {
       if(startPressed){
        oneButton.isEnabled = false
        twoButton.isEnabled = false
        threeButton.isEnabled = false
            if number == 3{
                PassFailLabel.text = "Pass"
                PassFailLabel.textColor = .green
                //sleep(1)
                
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: testName)
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            else{
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                failCount = failCount + 1
                startButton.isEnabled = true
            }
            if(failCount == 2){
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                //sleep(1)
                let userDefaults = UserDefaults.standard
                print("false")
                markFail()
            }
            startPressed = false
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
}

func vibrate(){
    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
}

