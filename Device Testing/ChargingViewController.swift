//
//  ChargingViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer

class ChargingViewController: UIViewController {
    
    @IBOutlet var ChargingLabel: UILabel!
    @IBOutlet var ChargeImage: UIImageView!
    
    
    @IBOutlet var PassButton: UIButton!
    
    
    var seguePath = "SegueToMulti"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        MPVolumeView.setVolume(0.8)
        
        let myDevice = UIDevice()
        print(myDevice.isBatteryMonitoringEnabled)
        UIDevice.current.isBatteryMonitoringEnabled = true
        myDevice.isBatteryMonitoringEnabled = true
        print(myDevice.isBatteryMonitoringEnabled)
        
        if(UIDevice.current.batteryState == UIDeviceBatteryState.charging){
            ChargeImage.image = UIImage(named: "ChargingGreen.png")
            let userDefaults = UserDefaults.standard
            let storeNumber = userDefaults.integer(forKey: "StoreNumber")
            print(storeNumber)
            userDefaults.set("Pass", forKey: "charging")
            
            PassButton.isEnabled = true
        }
        if(UIDevice.current.batteryState == UIDeviceBatteryState.full){
            ChargeImage.image = UIImage(named: "ChargingGreen.png")
            let userDefaults = UserDefaults.standard
            let storeNumber = userDefaults.integer(forKey: "StoreNumber")
            print(storeNumber)
            userDefaults.set("Pass", forKey: "charging")
            
            PassButton.isEnabled = true
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChargingViewController.batteryStateDidChange),
                                               name: NSNotification.Name.UIDeviceBatteryStateDidChange,
                                               object: nil)
        
        
    }
    @objc func batteryStateDidChange(notification: NSNotification){
        let myDevice = UIDevice()
        print(myDevice.isBatteryMonitoringEnabled)
        print("In BattState")
        
        switch (UIDevice.current.batteryState) {
        case UIDeviceBatteryState.unplugged:
            //ChargingLabel.text = "Not Charging"
            break;
        case UIDeviceBatteryState.charging:
            //ChargingLabel.text = "Charging"
            ChargeImage.image = UIImage(named: "ChargingGreen.png")
            
            PassButton.isEnabled = true
            break;
        case UIDeviceBatteryState.full:
            //ChargingLabel.text = "Fully Charged"
            break;
        case UIDeviceBatteryState.unknown:
            //ChargingLabel.text = "Unkown"
            break;
        
        
        default:
        break;
        }
        //myDevice.isBatteryMonitoringEnabled = true
 
    }
    
    
    @IBAction func FailButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "charging")
        sendFail(FailItem: "Charging")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        if(singleTest){
            self.dismiss(animated: true, completion: nil)
        }
        else{
            let storeNumber = userDefaults.integer(forKey: "StoreNumber")
            print(storeNumber)
            if(storeNumber == 1){
                performSegue(withIdentifier: "SegueToWRT", sender: self)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    @IBAction func PassButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: "charging")
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            let storeNumber = userDefaults.integer(forKey: "StoreNumber")
            print(storeNumber)
            if(storeNumber == 1){
                performSegue(withIdentifier: "SegueToWRT", sender: self)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    func shouldRun(){
        let testName = "Charging"
        let userDefaults = UserDefaults.standard
        let testList: [String: Bool] = userDefaults.dictionary(forKey: "testList") as! [String : Bool]
        let currentTestValue = testList.index(forKey: testName)
        
        
//        if(!currentTestValue){
//            self.performSegue(withIdentifier: seguePath, sender: self)
//        }
    }
}

