//
//  GameScene.swift
//  Check
//
//  Created by Conor Pierce on 7/8/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import SpriteKit
import GameplayKit

protocol GameSceneDelegate: class{
    func segue()
}

class GameScene: SKScene {
    
    weak var gameDelegate: GameSceneDelegate?
    
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    private var nodes: Int = 0
    var activeSlicePoints = [CGPoint]()
    var activeSlice: SKShapeNode!
    
    override func didMove(to view: SKView) {
        print("in view")
        createSlices()
        // Get label node from scene and store it for use later
        
        // Create shape node to use during mouse interaction
        print(self.frame.size.height)
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        print(model ?? "N/A")
        var w = (self.size.width)/10
        var nodeWidth = w
        var nodeHeight = w/2
        var height = self.size.height*0.65
        var width = self.size.width
        var bottom = -height/2
        var top = height/2
        var left = (-width/2 + nodeWidth)
        var right = width/2 - nodeWidth
        var ratio = height/width*1.1
        var topBar = height/2
        nodes = 0
        switch model {
        case "iPhone X":
             w = (self.size.width)/10
             nodeWidth = w
             nodeHeight = self.size.height/20
             height = self.size.height
             width = self.size.width
             bottom = -height/2 + nodeHeight
             top = height/2 - nodeHeight
             left = (-width/2 + nodeWidth*1.5)
             right = width/2 - nodeWidth*1.5
             ratio = 0.3125
             
            topBar = top-nodeHeight
        case "iPhone 8 Plus":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
            
        case "iPhone 7 Plus":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
           topBar = top - nodeWidth*0.45
        case "iPhone 6S Plus":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
        case "iPhone 6 Plus":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
        case "iPhone 6":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
        case "iPhone 6s":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
        case "iPhone 7":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
        case "iPhone 8":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top - nodeWidth*0.45
        case "iPad Pro 12.9 Inch":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top
        case "iPad Air 2":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top - nodeWidth*2
        case "iPad Air":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top - nodeWidth*2
        case "iPad 5":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top - nodeWidth*2
        case "iPad 6":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top - nodeHeight
        case "iPad Mini":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top
        case "iPad 2":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top
        case "iPad Pro 12.9 Inch 2. Generation":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*2
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top - nodeHeight
        case "iPad Pro 10.5 Inch":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top
        case "iPad Pro 9.7 Inch":
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*3
            top = height/2 - nodeHeight*3
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.5833
            
            topBar = top
        default:
            w = (self.size.width)/10
            nodeWidth = w
            nodeHeight = self.size.height/20
            height = self.size.height
            width = self.size.width
            
            bottom = -height/2 + nodeHeight*0.5
            top = height/2 - nodeHeight*0.5
            left = (-width/2) + nodeWidth*0.5
            right = width/2  - nodeWidth*0.5
            
            ratio = 0.4117
            
            topBar = top
        }
        
        
        
        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: nodeWidth, height: nodeHeight), cornerRadius: w * 0.01)
        
        if let spinnyNode = self.spinnyNode {
            spinnyNode.lineWidth = 2.5
            spinnyNode.name = "yes"
            spinnyNode.alpha = 0.8
            
            spinnyNode.zPosition = 100
            /*spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                            SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))*/
        }
        
        print(self.size.height)
        print(self.size.width)
        
        //Left Stripe
        for i in stride(from: bottom, to: top, by: nodeHeight){
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = CGPoint(x: (left),y: CGFloat(i))
                
                n.strokeColor = SKColor.black
                //
                nodes += 1
                self.addChild(n)
            }
        }
        //Right Stripe
        for i in stride(from: bottom, to: top, by: nodeHeight){
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = CGPoint(x: (right),y: CGFloat(i))
                
                n.strokeColor = SKColor.black
                nodes += 1
                self.addChild(n)
            }
        }
        //positive diag
        var count: CGFloat = left + nodeWidth
        for i in stride(from: bottom+nodeHeight, to: top-nodeHeight, by: nodeHeight){
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = CGPoint(x: -CGFloat(count),y: CGFloat(i))
                count += nodeWidth*ratio
                n.strokeColor = SKColor.black
                nodes += 1
                self.addChild(n)
            }
        }
        //Negative Diag
        
        count = left + nodeWidth
        for i in stride(from: bottom+nodeHeight, to: top-nodeHeight, by: nodeHeight){
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = CGPoint(x: CGFloat(count),y: CGFloat(i))
                count += nodeWidth*ratio
                
                n.strokeColor = SKColor.black
                nodes += 1
                self.addChild(n)
            }
        }
        //Top Stripe
        for i in stride(from: (left+nodeWidth), to: (right), by: nodeWidth){
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = CGPoint(x: CGFloat(i),y: topBar)
                
                n.strokeColor = SKColor.black
                nodes += 1
                self.addChild(n)
            }
        }
        //Bottom Stripe
        for i in stride(from: (left+nodeWidth), to: (right), by: nodeWidth){
            if let n = self.spinnyNode?.copy() as! SKShapeNode? {
                n.position = CGPoint(x: CGFloat(i),y: (bottom))
                
                n.strokeColor = SKColor.black
                nodes += 1
                self.addChild(n)
            }
        }
 
    }
    func createSlices() {
        activeSlice = SKShapeNode()
        activeSlice.strokeColor = .black
        //activeSlice.fillColor = .black
        activeSlice.lineWidth = 2
        //activeSlice.zPosition =
        addChild(activeSlice)
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        activeSlicePoints.removeAll(keepingCapacity: true)
        for touch in touches {
            let location = touch.location(in: self)
            let touchedNode = self.atPoint(location)
            if touchedNode.name == "yes"{
                let touchedNode1 = touchedNode as! SKShapeNode
                touchedNode1.fillColor = SKColor.green
                //touchedNode.removeFromParent()
                touchedNode.name = "no"
                nodes -= 1
            }
            
            
            activeSlicePoints.append(location)
            redrawActiveSlice()
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(nodes <= 1){
            let userDefaults = UserDefaults.standard
            
            userDefaults.set("Pass", forKey: "TouchGrid")
            self.gameDelegate?.segue()
            
        }
        for touch in touches {
            
            let location = touch.location(in: self)
            let touchedNode = self.atPoint(location)
            
            
            if touchedNode.name == "yes"{
                let touchedNode1 = touchedNode as! SKShapeNode
                touchedNode1.fillColor = SKColor.green
                //touchedNode.removeFromParent()
                touchedNode.name = "no"
                nodes -= 1
                print(nodes)
                //touchedNode.removeFromParent()
            }
            activeSlicePoints.append(location)
            redrawActiveSlice()
        }
        
        
        
    }
    func redrawActiveSlice() {
            let path = UIBezierPath()
            path.move(to: activeSlicePoints[0])
            for i in 1 ..< activeSlicePoints.count {
                path.addLine(to: activeSlicePoints[i])
            }
            activeSlice.path = path.cgPath
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
         createSlices()
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}

