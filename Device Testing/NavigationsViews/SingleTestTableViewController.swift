//
//  SingleTestTableViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 9/17/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit

class SingleTestTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "SingleTest")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
   
    

    
  

}
