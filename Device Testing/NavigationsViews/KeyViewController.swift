//
//  KeyViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 8/27/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import Firebase
import CoreTelephony
import MediaPlayer //Only for hidding  Volume view
import Crashlytics
import CoreNFC

class KeyViewController: UIViewController, NFCNDEFReaderSessionDelegate {

    @IBOutlet var viewBox: UIView!
    @IBOutlet var TextBox: UITextField!
    @IBOutlet var buildLabel: UILabel!
    var totalUpdated = false
    var nfcSession: NFCNDEFReaderSession?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBox.layer.cornerRadius = 10.0
        viewBox.clipsToBounds = true
        viewBox.layer.shadowColor = UIColor.black.cgColor
        viewBox.layer.shadowRadius = 3.0
        viewBox.layer.shadowOpacity = 1.0
        viewBox.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewBox.layer.masksToBounds = false
//        TextBox.layer.shadowColor = UIColor.black.cgColor
//        TextBox.layer.shadowRadius = 3.0
//        TextBox.layer.shadowOpacity = 1.0
//        TextBox.layer.shadowOffset = CGSize(width: 1, height: 1)
//        TextBox.layer.masksToBounds = false
        
        setTestList()

        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 0))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        
        // Do any additional setup after loading the view.
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String {
            //print(text)
            buildLabel.text = "Version: \(text)"
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewDidAppear(_ animated: Bool) {
        let alert = UIAlertController(title: "Please ensure that you have checked the following items", message: "Touch ID or Face ID is currently turned on with a passcode \n \n The phone is not on silent mode ", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
        }))
        
        self.present(alert, animated: true)
        print(getIPAddress())
        
        
    }
    
    @IBAction func ButtonPressed(_ sender: Any) {
        //Crashlytics.sharedInstance().crash()
        if(TextBox.text == "CPIERCEToken"){
            self.performSegue(withIdentifier: "SegueToToken", sender: self)
        }
        //buttonAction()
        logIn(key: (TextBox.text ?? "n/a"))
    }
    func NFC(){
        nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        nfcSession?.begin()
    }
    @IBAction func ReturnPressed(_ sender: Any) {
       // buttonAction()
        logIn(key: (TextBox.text ?? "n/a"))
    }
    
    
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    NFC()
                   
                }
            }
        }
    }
    
    func buttonAction(){
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        db.collection("users").whereField("Key", isEqualTo: TextBox.text ?? "n/a")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if((querySnapshot?.isEmpty)!){
                        print("Does not Exist")
                        let alert = UIAlertController(title: "Invalid Key", message: "Please enter a valid key to continue.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                    }
                    for document in querySnapshot!.documents {
                        
                        //print("\(document.documentID) => \(document.data())")
                        let userDefaults = UserDefaults.standard
                        let storeNumber = document.data()["StoreNumber"]as! Int
                        //print(storeNumber)
                        let active = document.data()["Active"]as! Bool
                        var unlocks = document.data()["Unlocks"]as! Int
                        if(active){
                            self.updateTotal()
                            userDefaults.set(storeNumber, forKey: "StoreNumber")
                            userDefaults.set(document.documentID, forKey: "DocID")
                            let  docRef = db.collection("users").document(document.documentID);
                            unlocks += 1
                            docRef.updateData([
                                "Unlocks": unlocks
                            ]) { err in
                                if let err = err {
                                    print("Error updating document: \(err)")
                                    let alert = UIAlertController(title: "Error Sending Unlock Data", message: "Please let Conor know if you see this message. Tell him this: \(document.documentID)", preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                    
                                    self.present(alert, animated: true)
                                    
                                } else {
                                    print("Document successfully updated")
                                }
                            }
                            let docID = userDefaults.string(forKey: "DocID")
                            print("This is the document ID: \(docID)")
                            self.performSegue(withIdentifier: "SegueToStart", sender: self)
                        }
                        else{
                            let alert = UIAlertController(title: "Your account is not active.", message: "Please email subscription@iostest.tools to renew your subscription", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                            
                            self.present(alert, animated: true)
                        }
                    }
                }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    func updateTotal(){
        
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        let  docRef = db.collection("TotalUsage").document("Usage");
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                var unlocks = document.data()!["TotalUnlocks"] as! Int
                if(self.totalUpdated){
                    return
                }
                self.totalUpdated = true
                unlocks += 1
                
                docRef.updateData([
                "TotalUnlocks": unlocks,
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Document successfully updated")
                    }
                }
                
            } else {
                print("Document does not exist TU")
            }
        }
    }
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("The session was invalidated: \(error.localizedDescription)")
    }
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        var result = ""
        for payload in messages[0].records {
            result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)! // 1
        }
        logIn(key: result)
        
    }
    func logIn(key: String){
        DispatchQueue.main.async {
            let db = Firestore.firestore()
            let settings = db.settings
            settings.areTimestampsInSnapshotsEnabled = true
            db.settings = settings
            
            db.collection("users").whereField("Key", isEqualTo: key ?? "n/a")
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        if((querySnapshot?.isEmpty)!){
                            print("Does not Exist")
                            let alert = UIAlertController(title: "Invalid Key", message: "Please enter a valid key to continue.", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                            
                            self.present(alert, animated: true)
                        }
                        for document in querySnapshot!.documents {
                            
                            //print("\(document.documentID) => \(document.data())")
                            let userDefaults = UserDefaults.standard
                            let storeNumber = document.data()["StoreNumber"]as! Int
                            //print(storeNumber)
                            let tokenCount = document.data()["Tokens"]as! Int
                            
                            userDefaults.set(tokenCount, forKey: "Tokens")
                            //print("Tokens Remaining: \(tokenCount)")
                            var unlocks = document.data()["Unlocks"]as! Int
                            if(tokenCount > 0){
                                self.updateTotal()
                                userDefaults.set(storeNumber, forKey: "StoreNumber")
                                userDefaults.set(document.documentID, forKey: "DocID")
                                let  docRef = db.collection("users").document(document.documentID);
                                unlocks += 1
                                docRef.updateData([
                                    "Unlocks": unlocks
                                ]) { err in
                                    if let err = err {
                                        print("Error updating document: \(err)")
                                        let alert = UIAlertController(title: "Error Sending Unlock Data", message: "Please let Conor know if you see this message. Tell him this: \(document.documentID)", preferredStyle: .alert)
                                        
                                        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                        
                                        self.present(alert, animated: true)
                                        
                                    } else {
                                        print("Document successfully updated")
                                    }
                                }
                                let docID = userDefaults.string(forKey: "DocID")
                                //print("This is the document ID: \(docID)")
                                self.performSegue(withIdentifier: "SegueToStart", sender: self)
                            }
                            else{
                                let alert = UIAlertController(title: "No Tokens remaining", message: "Please purchase  more Tokens to continue", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                
                                self.present(alert, animated: true)
                            }
                        }
                    }
            }
        }
    }

}

func getIPAddress() -> String? {
    var address: String?
    var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while ptr != nil {
            defer { ptr = ptr?.pointee.ifa_next }
            
            let interface = ptr?.pointee
            let addrFamily = interface?.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
    }
    return address
}

