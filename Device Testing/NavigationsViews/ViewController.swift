//
//  ViewController.swift
//  Device Testing
//
//  Created by Conor Pierce on 7/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
//import DeviceKit
import Firebase
import FirebaseAuth
import CoreTelephony
import LocalAuthentication

extension String {
    func luhnCheck() -> Bool {
        var sum = 0
        let reversedCharacters = self.reversed().map { String($0) }
        for (idx, element) in reversedCharacters.enumerated() {
            guard let digit = Int(element) else { return false }
            switch ((idx % 2 == 1), digit) {
            case (true, 9): sum += 9
            case (true, 0...8): sum += (digit * 2) % 9
            default: sum += digit
            }
        }
        return sum % 10 == 0
    }
}


class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var Label: UILabel!
    @IBOutlet var viewBox: UIView!
    @IBOutlet var WOField: UITextField!
    @IBOutlet var IMEIfield: UITextField!
    @IBOutlet var SubmitButton: UIButton!
    @IBOutlet var IQCButton: UIButton!
    
    @IBOutlet var IMEILabel: UILabel!
    @IBOutlet var SingleTestButton: UIButton!
    @IBOutlet var TokenLabel: UILabel!
    
    var imei = false
    var storeNumber = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewBox.layer.cornerRadius = 10.0
        viewBox.clipsToBounds = true
        viewBox.layer.shadowColor = UIColor.black.cgColor
        viewBox.layer.shadowRadius = 3.0
        viewBox.layer.shadowOpacity = 1.0
        viewBox.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewBox.layer.masksToBounds = false
        checkforIMEI()
        let userDefaults = UserDefaults.standard
        storeNumber = userDefaults.integer(forKey: "StoreNumber")
        
        setup()
        
        
        
        
        
        //Check the clipboard on page loaded
        NotificationCenter.default.addObserver(self, selector:#selector(CheckClipboard), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        Auth.auth().signInAnonymously() { (authResult, error) in
            // ...
        }
        
        
        
        
        //        let networkInfo = CTTelephonyNetworkInfo()
        //        let carrier = networkInfo.subscriberCellularProvider
        //
        //        // Get carrier name
        //        let carrierName = carrier?.carrierName
        //        print(carrierName)
        
        
        //let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        
        if (model == "iPhone 4" ||
            model == "iPhone 4s" ||
            model == "iPhone 5" ||
            model == "iPhone 5s" ||
            model == "iPhone 6"  ||
            model == "iPhone SE" ||
            model == "iPhone 6 Plus"  ||
            model == "iPhone 6s" ||
            model == "iPhone 6s Plus" ||
            model == "iPhone 7" ||
            model == "iPhone 7 Plus" ||
            model == "iPhone 8" ||
            model == "iPhone 8 Plus" ||
            model == "iPhone X" ||
            model == "iPhone XS" ||
            model == "iPhone XS Max" ||
            model == "iPhone XR" ||
            model == "iPad Mini Cellular" ||
            model == "iPad Mini 2 Cellular" ||
            model == "iPad Mini 3 Cellular" ||
            model == "iPad Mini 4 Cellular" ||
            model == "iPad Pro 9.7 Inch Cellular" ||
            model == "iPad Pro 12.9 Inch Cellular" ||
            model == "iPad Pro 12.9 Inch 2. Generation Cellular" ||
            model == "iPad Pro 10.5 Inch Cellular")
        
            
        {
            imei = true
            userDefaults.set(imei, forKey: "CellularModel")
        }
        else{
            imei = false
            userDefaults.set(imei, forKey: "CellularModel")
            IMEIfield.placeholder = "Enter Serial Number"
            IMEILabel.alpha = 0
        }
        
        
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(_ animated: Bool) {
        print(UIPasteboard.general.string ?? "N/A")
        checkID()
        let userDefaults = UserDefaults.standard
         let Tokens = userDefaults.integer(forKey: "Tokens")
        TokenLabel.text = "\(Tokens) Tokens"
        if let value = UIPasteboard.general.string {
            // there is value in clipboard
            
            let arr = value.components(separatedBy: .whitespaces)
            let result = arr.joined() // "3333333333"
            let myString = String(result.prefix(16))
            UIPasteboard.general.string = myString
            if(result.luhnCheck()){
                let alert = UIAlertController(title: "There is an IMEI in your clipboard", message: "Would you like to paste it now?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.IMEIfield.text = result
                    self.checkBoxes()
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
                
            }
            print(result)
            print("Clipboard Value: ")
        } else {
            // clipboard is empty
            print("nothing in clipboard")
        }
    }
    @objc func CheckClipboard()-> Void{
        if let value = UIPasteboard.general.string {
            // there is value in clipboard
            let arr = value.components(separatedBy: .whitespaces)
            let result = arr.joined() // "3333333333"
            let myString = String(result.prefix(16))
            UIPasteboard.general.string = myString
            
            if(result.luhnCheck()){
                let alert = UIAlertController(title: "There is an IMEI in your clipboard", message: "Would you like to paste it now?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.IMEIfield.text = result
                    self.checkBoxes()
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
                
            }
            print(result)
            print("Clipboard Value: ")
        } else {
            // clipboard is empty
            print("nothing in clipboard")
        }
    }
    func checkBoxes(){
        var text: String?
        var WOtext: String?
        var IMEIText: String?
        
        WOtext = WOField.text
        IMEIText = IMEIfield.text
        text = IMEIfield.text
        IMEILabel.text = ("\(text!.count)/15")
        IMEIfield.delegate = self
        if(imei){
            IMEILabel.text = ("\(text!.count)/15")
            if(text?.count == 15){
                
                if (text?.luhnCheck())!{
                    IQCButton.isEnabled = true
                    
                    if((WOtext?.count)! >= 7){
                        SubmitButton.isEnabled = true
                        IQCButton.isEnabled = true
                    }
                    else{
                        SubmitButton.isEnabled = false
                        //IQCButton.isEnabled = false
                    }
                    IMEILabel.textColor = .blue
                }
                else{
                    SubmitButton.isEnabled = false
                    IQCButton.isEnabled = false
                    IMEILabel.textColor = .gray
                }
            }
            else{
                SubmitButton.isEnabled = false
                IQCButton.isEnabled = false
                IMEILabel.textColor = .gray
            }
        }
            //If the device does not have an IMEI
        else{
            if((WOtext?.count)! >= 7 && (IMEIText?.count)! >= 12){
                SubmitButton.isEnabled = true
                IQCButton.isEnabled = true
            }
            else{
                SubmitButton.isEnabled = false
                IQCButton.isEnabled = false
            }
        }

    }
    
    @IBAction func IMEICheck(_ sender: Any) {
        var text: String?
        var WOtext: String?
        var IMEIText: String?
        
        WOtext = WOField.text
        IMEIText = IMEIfield.text
        text = IMEIfield.text
        IMEIfield.delegate = self
        if(imei){
        IMEILabel.text = ("\(text!.count)/15")
        if(text?.count == 15){
            
            if (text?.luhnCheck())!{
                IQCButton.isEnabled = true
                if((WOtext?.count)! >= 7){
                    SubmitButton.isEnabled = true
                    IQCButton.isEnabled = true
                }
                IMEILabel.textColor = .blue
            }
            else{
                SubmitButton.isEnabled = false
                IQCButton.isEnabled = false
                IMEILabel.textColor = .gray
            }
        }
        else{
            SubmitButton.isEnabled = false
            IQCButton.isEnabled = false
            IMEILabel.textColor = .gray
        }
    }
            //If the device does not have an IMEI
        else{
            if((WOtext?.count)! >= 7 && (IMEIText?.count)! >= 12){
                SubmitButton.isEnabled = true
                IQCButton.isEnabled = true
            }
            else{
                SubmitButton.isEnabled = false
                IQCButton.isEnabled = false
            }
        }
    }
    
    @IBAction func WOFieldAction(_ sender: Any) {
        var IMEIText: String?
        IMEIText = IMEIfield.text
        
        print("Store Number: \(storeNumber)")
        
        if(storeNumber == 1){
            SingleTestButton.alpha = 1
        }
        var WOtext: String?
        WOtext = WOField.text
        if(imei){
            var text: String?
            textField(IMEIfield, shouldChangeCharactersIn: NSMakeRange(14,20), replacementString: "")
            
            text = IMEIfield.text
            IMEILabel.text = ("\(text!.count)/15")
            if(text?.count == 15){
                
                if (text?.luhnCheck())!{
                    if((WOtext?.count)! >= 7){
                        SubmitButton.isEnabled = true
                        IQCButton.isEnabled = true
                    }
                    else{
                        SubmitButton.isEnabled = false
                        IQCButton.isEnabled = false
                    }
                    IMEILabel.textColor = .blue
                }
                else{
                    SubmitButton.isEnabled = false
                    IQCButton.isEnabled = false
                    IMEILabel.textColor = .gray
                }
            }
            else{
                SubmitButton.isEnabled = false
                IQCButton.isEnabled = false
                IMEILabel.textColor = .gray
            }
        }
        else{
            if((WOtext?.count)! >= 7 && (IMEIText?.count)! >= 12){
                SubmitButton.isEnabled = true
                IQCButton.isEnabled = true
            }
            else{
                SubmitButton.isEnabled = false
                IQCButton.isEnabled = false
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func WOButton(_ sender: Any) {
        var WOText: String?
        WOText = WOField.text
        
        var IMEItext: String?
        IMEItext = IMEIfield.text
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(WOText!, forKey: "WO")
        if(imei){
            userDefaults.set(IMEItext!, forKey: "IMEI")
        }
        else{
            userDefaults.set(IMEItext!, forKey: "Serial")
        }
        logID()
        
        
    }
    @IBAction func IQCButton(_ sender: Any) {
        var WOText: String?
        WOText = WOField.text
        
        var IMEItext: String?
        IMEItext = IMEIfield.text
        
        let userDefaults = UserDefaults.standard
        if(WOText != nil){
            userDefaults.set(WOText!, forKey: "WO")
        }
        else{
            userDefaults.set("N/A", forKey: "WO")
        }
        if(imei){
            userDefaults.set(IMEItext!, forKey: "IMEI")
        }
        else{
            userDefaults.set(IMEItext!, forKey: "Serial")
        }
        //logID()
        
        performSegue(withIdentifier: "SegueToIQC", sender: self)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= 15 // Change limit based on your requirement.
    }
    func setup(){
        clearLog()
        setModelName()
        setTestList()
        checkID()
    }
    func checkID(){
        
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        var testName = "IDAuth"
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                var authError: NSError?
                if LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                    print("Test3")
                    let userDefaults = UserDefaults.standard
                    userDefaults.set("Pass", forKey: "IDAuth")
                    // do your thing dependent on touch id being useable on the device
                }
                else{
                    print("Test4")
                    let alert = UIAlertController(title: "Please recheck TouchID", message: "Touch ID will fail on this test, double check that it is enabled with a passcode to proceed.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                    }))
                    
                    self.present(alert, animated: true)
                    
                }
                break
            }
        }
        
    
        
        
    }
    func logID(){
        let userDefaults = UserDefaults.standard
        userDefaults.set(Date(), forKey: "StartTime")
        
        var IMEI: String?
        if(imei){
             IMEI = userDefaults.string(forKey: "IMEI")
        }
        else{
             IMEI = userDefaults.string(forKey: "Serial")
        }
        let WO = userDefaults.string(forKey: "WO")
        print(WO)
        var Model = userDefaults.string(forKey: "model")
        //storeNumber = userDefaults.integer(forKey: "StoreNumber")
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
            
            let  docRef = db.collection("Devices").document(IMEI!);
            docRef.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                  
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": self.storeNumber,
                           
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                //print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef.setData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": self.storeNumber,
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                //print("Document added with ID: \(ref!.documentID)")
                            }
                        }
                        //print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
            let  docRef1 = db.collection("Devices/\(IMEI!)/Workorders").document(WO!);
            docRef1.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef1.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "PassCount": 0,
                            "FailCount": 0,
                            "Result": "N/A",
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A",
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef1.setData([
                            "Result": "N/A",
                            "StartTime": Date(),
                            "PassCount": 0,
                            "FailCount": 0,
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A"
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                //print("Document added with ID: \(ref!.documentID)")
                            }
                        }
                        print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
        }
    }
    func logIQCID(){
        let userDefaults = UserDefaults.standard
        userDefaults.set(Date(), forKey: "StartTime")
        
        var IMEI: String?
        if(imei){
            IMEI = userDefaults.string(forKey: "IMEI")
        }
        else{
            IMEI = userDefaults.string(forKey: "Serial")
        }
        let WO = userDefaults.string(forKey: "WO")
        print(WO)
        var Model = userDefaults.string(forKey: "model")
        //storeNumber = userDefaults.integer(forKey: "StoreNumber")
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
            
            let  docRef = db.collection("Devices").document(IMEI!);
            docRef.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": self.storeNumber,
                            
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                //print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef.setData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": self.storeNumber,
                            ]) { err in
                                if let err = err {
                                    print("Error adding document: \(err)")
                                } else {
                                    //print("Document added with ID: \(ref!.documentID)")
                                }
                        }
                        //print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
            let  docRef1 = db.collection("Devices/\(IMEI!)/Workorders").document(WO!);
            docRef1.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef1.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "PassCount": 0,
                            "FailCount": 0,
                            "Result": "N/A",
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A",
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef1.setData([
                            "Result": "N/A",
                            "StartTime": Date(),
                            "PassCount": 0,
                            "FailCount": 0,
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A"
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                //print("Document added with ID: \(ref!.documentID)")
                            }
                        }
                        print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
        }
    }
    func checkforIMEI(){
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
        db.collection("Devices").whereField("UUID", isEqualTo: uuid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        
                        self.IMEIfield.text = document.documentID
                        db.collection
                        self.checkBoxes()
                        
                        db.collection("Devices/\(document.documentID)/Workorders").order(by: "StartTime", descending: true).limit(to: 1).getDocuments() { (querySnapshot1, err) in
                            if let err = err {
                                print("Error getting documents: \(err)")
                            } else {
                                for document1 in querySnapshot1!.documents {
                                    let oneDayAgo = Date() - 86400
                                    let timestamp = document1.data()["StartTime"] as! Timestamp
                                    let dateTimeStamp = timestamp.dateValue()
                                    //let converted = NSDate(timeIntervalSince1970: timestamp / 1000)

                                    if(dateTimeStamp > oneDayAgo as Date){
                                        print("\(document1.documentID) => \(document1.data())")
                                        self.WOField.text = document1.documentID
                                        self.checkBoxes()
                                    }
                                }
                            }
                        }
                        
                        print("\(document.documentID) => \(document.data())")
                    }
                }
        }
        
    }
        
    }
    
}
