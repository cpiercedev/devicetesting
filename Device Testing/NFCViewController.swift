import UIKit
import CoreNFC
import MediaPlayer

class NFCViewController: UIViewController, NFCNDEFReaderSessionDelegate {
    
    var testName = "NFC"
    
    
    var nfcSession: NFCNDEFReaderSession?
    var seguePath = "SegueToProx"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        //nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        //nfcSession?.begin()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("The session was invalidated: \(error.localizedDescription)")

        //performSegue(withIdentifier: "SegueToProx", sender: self)
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        // Parse the card's information
        let userDefaults = UserDefaults.standard
        userDefaults.set("Pass", forKey: testName)
        print("NFC Passed")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    

    @IBAction func FailButtonPressed(_ sender: Any) {
       
            if NFCNDEFReaderSession.readingAvailable {
                // Set up the NFC session
                nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: nil, invalidateAfterFirstRead: true)
                    nfcSession?.begin()
            } else {
                // Provide fallback option
                print("NFC Not Available")
                let alert = UIAlertController(title: "NFC is not functioning.", message: "The test for this functionality has failed.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                }))
                
                self.present(alert, animated: true)
                markFail()
            }
        
    
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for NFC", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: "NFC")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: "NFC")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }

}
