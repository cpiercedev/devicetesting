//
//  LoopBackTest.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/10/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import MediaPlayer

class LoopBackTest: UIViewController {
    
    var testName = "Loopback"
    
    
    let engine = AVAudioEngine()
    @IBOutlet var PassButton: UIButton!
    
    
    //Setting the default sequential path
    var seguePath = "SegueToHeadphone"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let volumeView = MPVolumeView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        view.addSubview(volumeView)
        
        MPVolumeView.setVolume(0.5)
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        
        let input = engine.inputNode
        let player2 = AVAudioPlayerNode()
        engine.attach(player2)
        
        let bus = 0
        let inputFormat = input.inputFormat(forBus: 0)
        
        engine.connect(player2, to: engine.mainMixerNode, format: inputFormat)
        
        input.installTap(onBus: bus, bufferSize: 512, format: inputFormat) { (buffer, time) -> Void in
            player2.scheduleBuffer(buffer)
        }
        
        try! engine.start()
        player2.play()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.PassButton.isEnabled = true
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        engine.stop()
        let input = engine.inputNode
        //let bus = 0
        engine.inputNode.removeTap(onBus: 0)
        //input.removeTap(onBus: 0)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        engine.stop()
        let input = engine.inputNode
        //let bus = 0
        engine.inputNode.removeTap(onBus: 0)
        input.removeTap(onBus: 0)
    }
    @IBAction func FailButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        engine.stop()
        let input = engine.inputNode
        let bus = 0
        engine.inputNode.removeTap(onBus: 0)
        input.removeTap(onBus: 0)
        
        
        userDefaults.set("Fail", forKey: "Loopback")
        sendFail(FailItem: "Loopback")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    @IBAction func PassButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        engine.stop()
        let input = engine.inputNode
        engine.inputNode.removeTap(onBus: 0)
        input.removeTap(onBus: 0)
        userDefaults.set("Pass", forKey: "Loopback")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func clearTap(){
        //CLSLogv("%@", getVaList(["Tap Cleared"]))
        engine.stop()
        let input = engine.inputNode
        input.removeTap(onBus: 0)
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    
}
extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume;
        }
    }
}
