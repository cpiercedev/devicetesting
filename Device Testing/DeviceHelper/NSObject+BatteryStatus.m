//
//  NSObject+BatteryStatus.m
//  DeviceTesting
//
//  Created by Conor Pierce on 7/10/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

#import "NSObject+BatteryStatus.h"
#import <Foundation/Foundation.h>
#include <dlfcn.h>

@implementation NSObject (BatteryStatus)


-(NSDictionary *) FCPrivateBatteryStatus
{
    static mach_port_t *s_kIOMasterPortDefault;
    static kern_return_t (*s_IORegistryEntryCreateCFProperties)(mach_port_t entry, CFMutableDictionaryRef *properties, CFAllocatorRef allocator, UInt32 options);
    static mach_port_t (*s_IOServiceGetMatchingService)(mach_port_t masterPort, CFDictionaryRef matching CF_RELEASES_ARGUMENT);
    static CFMutableDictionaryRef (*s_IOServiceMatching)(const char *name);
    
    static CFMutableDictionaryRef g_powerSourceService;
    static mach_port_t g_platformExpertDevice;
    
    static BOOL foundSymbols = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        void* handle = dlopen("/System/Library/Frameworks/IOKit.framework/Versions/A/IOKit", RTLD_LAZY);
        s_IORegistryEntryCreateCFProperties = dlsym(handle, "IORegistryEntryCreateCFProperties");
        s_kIOMasterPortDefault = dlsym(handle, "kIOMasterPortDefault");
        s_IOServiceMatching = dlsym(handle, "IOServiceMatching");
        s_IOServiceGetMatchingService = dlsym(handle, "IOServiceGetMatchingService");
        
        if (s_IORegistryEntryCreateCFProperties && s_IOServiceMatching && s_IOServiceGetMatchingService) {
            g_powerSourceService = s_IOServiceMatching("IOPMPowerSource");
            g_platformExpertDevice = s_IOServiceGetMatchingService(*s_kIOMasterPortDefault, g_powerSourceService);
            foundSymbols = (g_powerSourceService && g_platformExpertDevice);
        }
    });
    
    if (! foundSymbols) return nil;
    
    CFMutableDictionaryRef prop = NULL;
    s_IORegistryEntryCreateCFProperties(g_platformExpertDevice, &prop, 0, 0);
    return prop ? ((NSDictionary *) CFBridgingRelease(prop)) : nil;
}
@end
