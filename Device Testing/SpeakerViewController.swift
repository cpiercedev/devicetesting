//
//  SpeakerViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/8/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import AVFoundation
import MediaPlayer

var player: AVAudioPlayer?

class SpeakerViewController: UIViewController{
    var number = 0
    var failCount = 0
    var startPressed = false
    var sounds = [String]()
    @IBOutlet var startButton: UIButton!
    //Setting the default sequential path
    var seguePath = "SegueToRearCamera"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.isEnabled = true
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        
        self.view.addSubview(volumeView)
        //MPVolumeView.setVolume(0.8)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        sounds.append("bird")
        sounds.append("train")
        sounds.append("rain")
    }
    
    @IBAction func VibrationStartPressed(_ sender: Any) {
        startPressed = true
        startButton.isEnabled = false
        number = Int.random(in: 0..<3)
        playSound(sound: sounds[number])
        
        print("Button Pressed")
        
        
    }
    @IBAction func birdPressed(_ sender: Any) {
        if(startPressed){
            player?.stop()
            if number == 0{
                player?.stop()
                player = nil
                startButton.isEnabled = true
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Speakers")
                segue()
            }
            else{
                startButton.isEnabled = true
                failCount = failCount + 1
                
            }
            if(failCount == 2){
               
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Fail", forKey: "Speakers")
                segue()
            }
            startPressed = false
        }
    }
    @IBAction func trainPressed(_ sender: Any) {
        if(startPressed){
            player?.stop()
            if number == 1{
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Speakers")
                segue()
                
            }
            else{
                failCount = failCount + 1
                startButton.isEnabled = true
            }
            if(failCount == 2){
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Fail", forKey: "Speakers")
                segue()
            }
            startPressed = false
        }
    }
    @IBAction func rainPressed(_ sender: Any) {
        if(startPressed){
            player?.stop()
            player = nil
            if number == 2{
                player?.stop()
                do {
                    try AVAudioSession.sharedInstance().setActive(false)
                }
                catch let error {
                    print(error.localizedDescription)
                }
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Speakers")
                segue()
            }
            else{
                startButton.isEnabled = true
                failCount = failCount + 1
            }
            if(failCount == 2){
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Fail", forKey: "Speakers")
                segue()
            }
            startPressed = false
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for Speaker", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func segue(){
         let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        //let storeNumber = userDefaults.integer(forKey: "StoreNumber")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
            
        }
    }
    func markNA(){
        sendNA(NAItem: "Speakers")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "Speakers")
        segue()
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        sendFail(FailItem: "Speakers")
        userDefaults.set("Fail", forKey: "Speakers")
       segue()
    }
}
func playSound(sound: String) {
    guard let url = Bundle.main.url(forResource: sound, withExtension: "wav") else { return }
    
    do {
        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try AVAudioSession.sharedInstance().setActive(true)
        
        
        
        /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
        
        /* iOS 10 and earlier require the following line:
         player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
        
        guard let player = player else { return }
        
        player.play()
        
    } catch let error {
        print(error.localizedDescription)
    }
}

