//
//  BluetoothViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 11/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//
import Foundation
import UIKit
import CoreBluetooth
import MediaPlayer

class BluetoothIQC: UIViewController, CBCentralManagerDelegate {
    @IBOutlet var testLabel: UILabel!
    @IBOutlet var testIcon: UIImageView!
    
    var testName = "Bluetooth"
    var seguePath = "SegueToAccelerometer"
    
    var manager:CBCentralManager!
    var result = false
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            result = true
            print("Bluetooth is on")
            break
        case .poweredOff:
            result = false
            print("Bluetooth is off")
            break
        case .resetting:
            print("Bluetooth is resetting.")
            break
        case .unauthorized:
            result = false
            break
        case .unsupported:
            result = false
            break
        case .unknown:
            result = false
            print("Bluetooth is unknown")
            break
        default:
            break
        }
        if(!result){
            let alert = UIAlertController(title: "Please turn bluetooth on.", message: "If bluetooth cannot be turned on, manually mark this test as failed.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
            }))
            
            self.present(alert, animated: true)
        }
        else{
            markPass()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        
        print("view did load")
        manager = CBCentralManager()
        manager.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view did appear")
        checkTestList()
        manager = CBCentralManager()
        manager.delegate = self
    }
    
    
    
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    //print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for Bluetooth", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    
    
    
    func markPass(){
        testLabel.text = "Passed"
        testIcon.image = UIImage(named: "BluetoothPass.png")
        print("\(testName) Passed")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let userDefaults = UserDefaults.standard
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            userDefaults.set("Pass", forKey: self.testName)
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("N/A", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    func markFail(){
        testLabel.text = "Failed"
        print("\(testName) Failed")
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    
    
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            print("Test Not available")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            testLabel.text = "Not Available"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if(singleTest){
                    self.dismiss(animated: true, completion: nil)
                    //self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
                else{
                    self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
            }
            
        }
        
    }
}
