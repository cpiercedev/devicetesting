//
//  ProxViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/11/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer //Only for hidding  Volume view

class ProximityIQC: UIViewController{
    
    @IBOutlet var ProxCountLabel: UILabel!
    var ProxCount = 0
    //Setting the default sequential path
    var seguePath = "SegueToIQCResults"
    override func viewDidLoad() {
        super.viewDidLoad()
        /* Setting the path based off whether or not the user is doing a single test or sequential*/
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
        
        
        let model = userDefaults.string(forKey: "model")
        if (model == "iPhone 4" || model == "iPhone 4s" || model == "iPhone 5" || model == "iPhone 5s" || model == "iPhone 6"  || model == "iPhone SE" || model ==  "iPhone 6 Plus"  || model == "iPhone 6s" || model == "iPhone 6s Plus" || model == "iPhone 7" || model == "iPhone 7 Plus" || model == "iPhone 8" || model == "iPhone 8 Plus" || model == "iPhone X") {
        }
        else{
            userDefaults.set("N/A", forKey: "Prox")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        
        UIDevice.current.isProximityMonitoringEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(proximityChanged), name: NSNotification.Name.UIDeviceProximityStateDidChange, object: nil)
        
        
    }
    
    @objc func proximityChanged()
    {
        if UIDevice.current.proximityState{
            print("Proximity true")
            ProxCount = ProxCount + 1
        } else {
            print("Proximity false")
            ProxCountLabel.text = String(ProxCount)
            if(ProxCount == 2){
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Prox")
                UIDevice.current.isProximityMonitoringEnabled = false
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
                
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for Proximity", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: "Prox")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "Prox")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "Prox")
        sendFail(FailItem: "Prox")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
}
