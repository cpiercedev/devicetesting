//
//  CellularViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 11/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer

class CellularIQC: UIViewController {
    @IBOutlet var testIcon: UIImageView!
    @IBOutlet var testLabel: UILabel!
    
    var testName = "Cellular"
    var seguePath = "SegueToWifi"
    
    var reachability: Reachability?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        networkCheck()
    }
    override func viewDidDisappear(_ animated: Bool) {
        stopNotifier()
    }
    func networkCheck(){
        startHost(at: 0)
        
    }
    
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for Network", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    
    func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index], useClosures: true)
        startNotifier()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.startHost(at: (index + 1) % 3)
        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
            // hostNameLabel.text = hostName
        } else {
            reachability = Reachability()
            //hostNameLabel.text = "No host name"
        }
        self.reachability = reachability
        // print("--- set up with host name: \(hostNameLabel.text!)")
        
        if useClosures {
            reachability?.whenReachable = { reachability in
                self.updateLabelColourWhenReachable(reachability)
            }
            reachability?.whenUnreachable = { reachability in
                self.updateLabelColourWhenNotReachable(reachability)
            }
        } else {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(reachabilityChanged(_:)),
                name: .reachabilityChanged,
                object: reachability
            )
        }
    }
    
    func startNotifier() {
        //print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
            // networkStatus.textColor = .red
            // networkStatus.text = "Unable to start\nnotifier"
            return
        }
    }
    
    func stopNotifier() {
        //print("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        //print("\(reachability.description) - \(reachability.connection)")
        if reachability.connection == .wifi {
            testLabel.text = "Please disconnect Wifi and connect to a Cellular Network, if not available manually mark the test."
        }
        else{
            markPass()
            // detailLabel.text = "Turn Wifi On and connect to a local Network, then return to this screen."
        }
    }
    
    //self.networkStatus.text = "\(reachability.connection)"
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        //print("\(reachability.description) - \(reachability.connection)")
        
        // self.networkStatus.textColor = .red
        
        //self.networkStatus.text = "\(reachability.connection)"
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none {
            updateLabelColourWhenReachable(reachability)
        } else {
            updateLabelColourWhenNotReachable(reachability)
        }
    }
    
    deinit {
        stopNotifier()
    }
    
    func markPass(){
        testLabel.text = "Passed"
        testIcon.image = UIImage(named: "NetworkEnabled.png")
        print("\(testName) Passed")
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("N/A", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    func markFail(){
        print("\(testName) Failed")
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            print("Test Not available")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            testLabel.text = "Not Available"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if(singleTest){
                    self.dismiss(animated: true, completion: nil)
                    //self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
                else{
                    self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
            }
            
        }
        
    }
}
